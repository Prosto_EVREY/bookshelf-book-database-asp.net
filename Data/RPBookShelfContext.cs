using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RPBookShelf.Models;

    public class RPBookShelfContext : DbContext
    {
        public RPBookShelfContext (DbContextOptions<RPBookShelfContext> options)
            : base(options)
        {
        }

        public DbSet<RPBookShelf.Models.Book> Book { get; set; } = default!;
    }

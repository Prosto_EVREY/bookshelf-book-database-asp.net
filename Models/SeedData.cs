﻿using System;
using Microsoft.EntityFrameworkCore;
using RPBookShelf;

namespace RPBookShelf.Models
{
	public class SeedData
	{
		public static void Initialize(IServiceProvider serviceProvider)
		{
			using (var context = new RPBookShelfContext(
				serviceProvider.GetRequiredService<
					DbContextOptions<RPBookShelfContext>>()))
			{
                if (context.Book.Any())
                {
                    return;   // DB has been seeded
                }

                context.Book.AddRange(
                    new Book
                    {
                        Title = "Harry Potter",
                        ReleaseDate = DateTime.Parse("1994-2-12"),
                        NumOfPages = 300,
                        Author = "Joahn Roaling",
                        Genre = "Fantasy",
                        Price = 7.99M
                    },

                    new Book
                    {
                        Title = "The White Fang",
                        ReleaseDate = DateTime.Parse("1984-3-13"),
                        NumOfPages = 255,
                        Author = "Jack London",
                        Genre = "Drama",
                        Price = 8.99M
                    },

                    new Book
                    {
                        Title = "The house of Air",
                        ReleaseDate = DateTime.Parse("1986-2-23"),
                        NumOfPages = 122,
                        Author = "Dmitriy Katkov",
                        Genre = "Comedy",
                        Price = 9.99M
                    },

                    new Book
                    {
                        Title = "War and Piec",
                        ReleaseDate = DateTime.Parse("1859-4-15"),
                        NumOfPages = 440,
                        Author = "Lev Tolstoy",
                        Genre = "Western",
                        Price = 3.99M
                    }
                );
                context.SaveChanges();
            }

		}

	}
}


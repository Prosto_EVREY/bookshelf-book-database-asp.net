﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace RPBookShelf.Models
{
	public class Book
	{
		public int Id { get; set; }

		[Required, StringLength(60, MinimumLength = 1)]
		public string Title { get; set; } = String.Empty;

		[Display(Name = "Release Date")]
		[DataType(DataType.Date)]
		public DateTime ReleaseDate { get; set; }

		[Required]
		public int NumOfPages { get; set; }

		[Required]
		[RegularExpression(@"^[A-Z]+[a-z0-9A-Z\s]*$")]
		public string Genre { get; set; } = String.Empty;

        [Required, StringLength(60, MinimumLength = 1)]
        public string Author { get; set; } = String.Empty;

        [Required, DataType(DataType.Currency)]
		[Column(TypeName = "decimal(18,2)")]
		public decimal Price { get; set; }
	}
}

